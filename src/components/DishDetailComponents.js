import React, {Component} from 'react';
import { Row, Label, Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class DishDetail extends Component {

  constructor(props) {
    super(props);

    this.state = {
        isModalOpen: false
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
     });
  }

  handleSubmit(values, dishId) {
        this.props.postComment(dishId, values.rating, values.author, values.comment);
    }

  render() {
    const DishDetail = ({dish}) => {
      return(
        <Card>
        <CardImg top src={baseUrl + dish.image} alt={dish.name} />
        <CardBody>
            <CardTitle>{dish.name}</CardTitle>
            <CardText>{dish.description}</CardText>
        </CardBody>
    </Card>
      );
    }

    const CommentForm = ({isModalOpen, dishId}) => {
      return(
        <Modal isOpen={isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Submit comment</ModalHeader>
          <ModalBody>
          <div className="col-12 col-md">
            <LocalForm onSubmit={(values) => this.handleSubmit(values, dishId)}>
                <Row className="form-group">
                    <Label htmlFor="raiting">Rating</Label>
                    <Control.select className="form-control" model=".raiting" id="raiting" name="raiting">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                    </Control.select>
                </Row>
                <Row className="form-group">
                    <Label htmlFor="author">Your name</Label>
                    <Control.text model=".author" id="author" name="author"
                        placeholder="Your name"
                        className="form-control"
                        validators={{
                            required, minLength: minLength(3), maxLength: maxLength(15)
                        }}
                    />
                    <Errors
                        className="text-danger"
                        model=".author"
                        show="touched"
                        messages={{
                            required: 'Required',
                            minLength: 'Must be greater than 2 characters',
                            maxLength: 'Must be 15 characters or less'
                        }}
                     />
                </Row>
                <Row className="form-group">
                    <Label htmlFor="comment">Comment</Label>
                    <Control.textarea model=".comment" id="comment" name="comment"
                            rows="12"
                            className="form-control" />
                </Row>
                <Row className="form-group">
                    <Button type="submit" color="primary">
                        Submit
                    </Button>
                </Row>
            </LocalForm>
          </div>
          </ModalBody>
        </Modal>
      );
    }

  const DishComments = ({commentsArray, postComment, dishId}) => {
    if (commentsArray != null) {
        const comments = commentsArray.map((comment) => {
          if (comment != null) {
            return(
              <li tag="li" className="m-1" key={comment.id}>
              <p>{comment.comment}</p>
              <p>-- {comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
            </li>
            );
          } else {
             return(<div></div>);
          }
        }); 
        return(
          <div>
            <h4>Comments</h4>
            <ul className="list-unstyled">
            {comments}
            </ul>
            <Button outline onClick={this.toggleModal}><span className="fa fa-pencil fa-lg"></span> Submit comment</Button>
            <CommentForm isModalOpen={this.state.isModalOpen} dishId={dishId} postComment={postComment}></CommentForm>
          </div>
          );
      } else {
        return(<div></div>);
      }
  }


  if (this.props.isLoading) {
      return(
          <div className="container">
              <div className="row">
                  <Loading />
              </div>
          </div>
      );
  } else if (this.props.errMessage) {
      return(
          <div className="container">
              <div className="row">
                  <h4>{this.props.errMessage}</h4>
              </div>
          </div>
      );
  } else {
      return (
        <div className="container">
          <div className="row">
              <Breadcrumb>
                  <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                  <BreadcrumbItem active>{this.props.dish.name}</BreadcrumbItem>
              </Breadcrumb>
              <div className="col-12">
                  <h3>{this.props.dish.name}</h3>
                  <hr />
              </div>
          </div>
          <div className="row">
              <div className="col-12 col-md-5 m-1">
                  <DishDetail dish={this.props.dish} />
              </div>
              <div className="col-12 col-md-5 m-1">
                  <DishComments commentsArray={this.props.comments} postComment={this.props.postComment} dishId={this.props.dish.id} />
              </div>
          </div>
        </div>
    );
  }
  }
}

export default DishDetail;